
<p id="timer"></p>

<script>
// Set the date we're counting down to
var now = new Date().getTime();
// var countDownDate = new Date(now).setMinutes(new Date(now).getMinutes() + 3 );

var countDownDate = new Date(now).setSeconds(new Date(now).getSeconds() + 60 );

// Update the count down every 0.1 second
var x = setInterval(function() {

  // Get today's date and time
  now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for minutes and seconds

  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.ceil((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="timer"
  document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";
    
  // If the count down is over, do this
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
 // put other code here for when timer reaches 0;

  }
}, 100);
</script>

<!-- <script>
function myFunction() {
  alert("it's been 3 seconds since loaded.");
}
</script>

 -->