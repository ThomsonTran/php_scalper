<?php

require_once("Event.class.php");
	class DBManager{

		
		private $db;

		public function __construct(){
			$host 	= "localhost";
			$user 	= "root";
			$pass 	= "";
			$dbname = "php2proj";

			//Tris to connect to the database using the provided credentials
			//if the connection works it will keep the persistance, else it will throw and error
			try{
				$this->db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

				/*TO SEE MYSQL ERRORS*/
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}catch(Exception $e){
				die("Database Connection Error: " . $e->getMessage());
			}
		}

		/**
		 * getAllEvents : Function to get all the events in the database and return an object
		 */
		public function getAllEvents(){
			$query 		= $this->db->query("SELECT * FROM events ORDER BY id DESC"); 
			$eventsArray = $query->fetchAll(PDO::FETCH_ASSOC);
			$eventsObj = [];

			foreach ($eventsArray as $array)
				$eventsObj[] = new Event($array);

			return $eventsObj;
		}

			/**
		 * getAllEvents : Function to get all the events in the database and return an object
		 */
		public function get18Events(){
			$query 		= $this->db->query("SELECT * FROM events  ORDER BY id DESC LIMIT 18"); 
			$eventsArray = $query->fetchAll(PDO::FETCH_ASSOC);
			$eventsObj = [];

			foreach ($eventsArray as $array)
				$eventsObj[] = new Event($array);

			return $eventsObj;
		}
		
		/**
		 * getSingleEvent : Function to get a single event in the database and return an object
		 */
		public function getSingleEvent($id){
			$query 	= $this->db->prepare("SELECT * FROM events WHERE id = :id");
			$query->execute(array($id));
			$result = $query->fetch(PDO::FETCH_ASSOC);

			if($result)
				return new Event($result);
			else
				return false;
		}

		/**
		 * [addEvent function to add a events in the database]
		 * @param [type] $events [event]
		 */
		public function addEvent($events){
	

			$query = $this->db->prepare("INSERT INTO events VALUES (DEFAULT, :name, :type, :dateEvent, :timeEvent, :location, :duration, :tickets, DEFAULT)");
			$query->execute($events);
		}


			/**
		 * [deleteEvent function to delete an event from the database]
		 * @param  [type] $id [the id of the event to delete]
		 * @return [type]     [true or false if the event was deleted or not]
		 */
		public function deleteEvent($id){
			$query = $this->db->prepare("DELETE FROM events WHERE id = :id");
			return $query->execute(array("id" => $id));
		}



		/**
		 * [editEvent function to edit a student in the database]
		 * @param  [type] $event [description]
		 * @return [type]          [T/F]
		 */


		public function editEvent($event){

			$array = $event -> toArray();
			$array['id2'] = $event-> getId();
			$query = $this->db->prepare("UPDATE events SET id = :id, name = :name, type = :type, dateEvent = :dateEvent, timeEvent = :timeEvent, location = :location, duration = :duration, tickets = :tickets, status = :status WHERE id = :id2");
			return $query->execute($array);
		}



			/**
		 * [buyTicket function to edit ticket number to ticket number - 1]
		 * @param  [type] $event [description]
		 * @return [type]          [T/F]
		 */


		public function buyTicket($eventid){

			$event = $this -> getSingleEvent($eventid);
			$currentTicket = $event -> getTickets();

			$event->setTickets($currentTicket - 1 );
			
			return $this -> editEvent($event);

		}



		public function searchFor($col,$entry){

			$sql = "SELECT * FROM events WHERE {$col} LIKE :entry";

			$entry = "%$entry%";

			$query 	= $this->db->prepare($sql);

			$query ->bindValue(':entry',$entry);

			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);


			if($result) {
				foreach ($result as $array)
				$eventsObj[] = new Event($array);
				return $eventsObj;
			}
			else{
				return false;
			}
		}		

		public function addClient($client){


		$query = $this->db->prepare("INSERT INTO clients VALUES (DEFAULT,:firstname, :lastname, :address, :province, :city, :postal, :telephone, :email, DEFAULT, :cc_number, :cc_type, :cc_expiration, :csc, :username, :password)");
			$query->execute($client);
		}

		public function getClient($username){


		$query = $this->db->prepare("SELECT * FROM clients WHERE username=?");
			$query->execute([$username]);
			$result = $query ->fetch(PDO::FETCH_ASSOC);
			return $result;
		}








	}



?>