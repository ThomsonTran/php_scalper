<?php 

class Event{

	public $id;
	public $name;
	public $type;
	public $dateEvent;
	public $timeEvent;
	public $location;
	public $duration;
	public $tickets;
	public $status;





		public function __construct($array){
			foreach ($array as $key => $value)
				 $this->{$key} = $value;
		}



public function toArray(){
            $array = [];
            $array['id'] = $this->id;
            $array['name'] = $this->name;
            $array['type'] = $this->type;
            $array['dateEvent'] = $this->dateEvent;
            $array['timeEvent'] = $this->timeEvent;
            $array['location'] = $this->location;
            $array['duration'] = $this->duration;
            $array['tickets'] = $this->tickets;
            $array['status'] = $this->status;
            return $array;
        }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * @param mixed $dateEvent
     *
     * @return self
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeEvent()
    {
        return $this->timeEvent;
    }

    /**
     * @param mixed $timeEvent
     *
     * @return self
     */
    public function setTimeEvent($timeEvent)
    {
        $this->timeEvent = $timeEvent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     *
     * @return self
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     *
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $tickets
     *
     * @return self
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}


 ?>