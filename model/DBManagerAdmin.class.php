<?php

require_once("Event.class.php");
require_once("Client.class.php");
	class DBManagerAdmin{

		
		private $db;

		public function __construct(){
			$host 	= "localhost";
			$user 	= "root";
			$pass 	= "";
			$dbname = "php2proj";

			//Tris to connect to the database using the provided credentials
			//if the connection works it will keep the persistance, else it will throw and error
			try{
				$this->db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

				/*TO SEE MYSQL ERRORS*/
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}catch(Exception $e){
				die("Database Connection Error: " . $e->getMessage());
			}
		}


    /**
     * Retrieves the user with the specified $id.
     * @param $id location on the database.
     * @return an array representing the user at the specified id, or false on failure.
     */
    public function getClient($username){
        $query = $this->db->prepare("SELECT * FROM clients WHERE username = ?");
        $result = $query->execute(array($username)) ? $query->fetch(PDO::FETCH_ASSOC) : false;
        return $result;
    }


    /**
     * Grant access to the user if username and password matches, and also if user is already valid.
     * @param $username
     * @param $password
     * @return bool|mixed
     */
    public function grantAccess($username, $password){
        $query = $this->db->prepare("SELECT * FROM clients WHERE username = ? AND password = ? AND status = 1");
        if($query->execute(array($username, $password))){
            if($query->fetch() == null)
                return false;
            else
                return true;
        }
    }

        /**
     * Validates the user in the Database to grant access .
     * @param $user
     * @return bool
     */
    public function validateClient($client){
        $query = $this->db->prepare("UPDATE users SET status = 1 WHERE id = ?");
        return $query->execute(array($user->getId()));
    }

			/**
		 * getAllEvents : Function to get all the events in the database and return an object
		 */
		public function get18Events(){
			$query 		= $this->db->query("SELECT * FROM events LIMIT 18"); 
			$eventsArray = $query->fetchAll(PDO::FETCH_ASSOC);
			$eventsObj = [];

			foreach ($eventsArray as $array)
				$eventsObj[] = new Event($array);

			return $eventsObj;
		}




	}

?>