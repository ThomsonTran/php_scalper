<?php 

class Client {

    //personal information

private $id;
private $firstname;
private $lastname;
private $address;
private $city;
private $province;
private $postal;
private $telephone;
private $email;
private $status;

    //credit card information
private $cc_number;
private $cc_type;
private $cc_expiration;
private $csc;

    //system info
private $username;
private $password;



public function __construct($array){
			foreach ($array as $key => $value)
				 $this->{$key} = $value;
		}



public function toArray(){
            $array = [];
            $array['id'] = $this->id;
            $array['firstname'] = $this->firstname;
            $array['lastname'] = $this->lastname;
            $array['address'] = $this->address;
            $array['city'] = $this->city;
            $array['province'] = $this->province;
            $array['postal'] = $this->postal;
            $array['telephone'] = $this->telephone;
            $array['email'] = $this->email;
            $array['status'] = $this->status;
            $array['cc_number'] = $this->cc_number;
            $array['cc_type'] = $this->cc_type;
            $array['cc_expiration'] = $this->cc_expiration;
            $array['csc'] = $this->csc;
            $array['username'] = $this->username;
            $array['password'] = $this->password;

            return $array;
        }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     *
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param mixed $province
     *
     * @return self
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostal()
    {
        return $this->postal;
    }

    /**
     * @param mixed $postal
     *
     * @return self
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     *
     * @return self
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcNumber()
    {
        return $this->cc_number;
    }

    /**
     * @param mixed $cc_number
     *
     * @return self
     */
    public function setCcNumber($cc_number)
    {
        $this->cc_number = $cc_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcType()
    {
        return $this->cc_type;
    }

    /**
     * @param mixed $cc_type
     *
     * @return self
     */
    public function setCcType($cc_type)
    {
        $this->cc_type = $cc_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcExpiration()
    {
        return $this->cc_expiration;
    }

    /**
     * @param mixed $cc_expiration
     *
     * @return self
     */
    public function setCcExpiration($cc_expiration)
    {
        $this->cc_expiration = $cc_expiration;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCsc()
    {
        return $this->csc;
    }

    /**
     * @param mixed $csc
     *
     * @return self
     */
    public function setCsc($csc)
    {
        $this->csc = $csc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
} ?>