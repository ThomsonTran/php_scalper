<?php 

require_once "model/Event.class.php";

class Eventpurchase extends Event{


public $quantity;




public function toArray(){
            $array = [];
            $array['id'] = $this->id;
            $array['name'] = $this->name;
            $array['type'] = $this->type;
            $array['dateEvent'] = $this->dateEvent;
            $array['timeEvent'] = $this->timeEvent;
            $array['location'] = $this->location;
            $array['duration'] = $this->duration;
            $array['tickets'] = $this->tickets;
            $array['status'] = $this->status;
            $array['quantity'] = $this->quantity;
            return $array;
        }



    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }
}


 ?>