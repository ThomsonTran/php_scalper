<?php 

require_once("model/DBManager.class.php");

require_once("model/Eventpurchase.class.php");
session_start();
$db = new DBManager();



if(isset($_POST['register'])) {

$_SESSION['newclient'] = array();

$_SESSION['newclient']['username'] = $_POST['username'];
$_SESSION['newclient']['firstname'] = $_POST['firstname'];
$_SESSION['newclient']['lastname'] = $_POST['lastname'];
$_SESSION['newclient']['address'] = $_POST['address'];
$_SESSION['newclient']['province'] = $_POST['province'];
$_SESSION['newclient']['city'] = $_POST['city'];
$_SESSION['newclient']['postal'] = $_POST['postal'];
$_SESSION['newclient']['telephone'] = $_POST['telephone'];
$_SESSION['newclient']['email'] = $_POST['email'];
$_SESSION['newclient']['cc_number'] = $_POST['cc_number'];
$_SESSION['newclient']['cc_type'] = $_POST['cc_type'];
$_SESSION['newclient']['cc_expiration'] = $_POST['cc_expiration'];
$_SESSION['newclient']['csc'] = $_POST['csc'];


// $_POST['username']
// $_POST['firstname']
// $_POST['lastname']
// $_POST['address']
// $_POST['province']
// $_POST['city']
// $_POST['postal']
//  $_POST['telephone']
// $_POST['email']
// $_POST['cc_number']
//  $_POST['cc_type']
// $_POST['cc_expiration']             
// $_POST['csc']



if(empty($_POST['username']) 	
	or empty($_POST['firstname'])
	or empty($_POST['lastname'])  
	or empty($_POST['address']) 
	or empty($_POST['province'])
	or empty($_POST['city']) 
	or empty($_POST['postal'])
	or empty($_POST['telephone']) 
	or empty($_POST['cc_number']) 
	or empty($_POST['cc_type']) 
	or empty($_POST['cc_expiration']) 
	or empty($_POST['csc']) 
	or empty($_POST['password'])) {

 header("location: index.php?controller=menu&action=registration&error=empty");

}else if(!empty($_POST['password']) && ($_POST['password'] != $_POST['confirmPassword']) )
	{

		header("location: index.php?controller=menu&action=registration&error=password");
	
}else{
unset($_POST['register']);
unset($_POST['confirmPassword']);
$db -> addClient($_POST);

header("location: index.php");

	
	}
}



if(isset($_POST['login'])) {


$username = $_POST['username'];
$password = $_POST['password'];
$user = $db ->getClient($username);

if ($user and ($user['password'] == $password)) {
	$_SESSION['user'] = $user;
	 header("location: index.php");
   
} else {
    header("location: index.php?controller=menu&action=login&error=yes");
} 


}

if(isset($_POST['addtocart'])) {

$event = $db -> getSingleEvent($_POST['cart']);
$testitem = new Eventpurchase($event->toArray());
$testitem -> setQuantity($_POST['quantity']);
$_SESSION['cart'][] = $testitem->toArray();
header("location: index.php");

}






 ?>