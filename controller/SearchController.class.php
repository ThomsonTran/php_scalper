<?php
class SearchController{
	private $db;

	function __construct(){
		$this->db = new DBManager();
	}

	function view(){
		$events = $this ->db -> searchFor($_POST['col'],$_POST['entry'] );

		require_once 'view/search.php';


	}


	function error(){
		require_once 'view/404.php';
	}
}

