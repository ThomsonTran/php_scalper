<?php
class IndexController{
	private $db;

	function __construct(){
		$this->db = new DBManager();
	}

	function view(){
		$events = $this ->db -> get18Events(); 
		require_once 'view/index.php';
	}


	function error(){
		require_once 'view/404.php';
	}


	
}

