<?php



class MenuController{
	private $db;

	function __construct(){
		$this->db = new DBManager();
	}


	function registration(){
		require_once 'view/registration.php';
	}

	function login(){
		require_once 'view/login.php';
	}

	function error(){
		require_once 'view/404.php';
	}

	function logout(){
		session_destroy();
		header("location: index.php");

	}

	function cart(){
	

		require_once 'view/cart.php';

	}

}

