<?php
  session_start();
	$controller = "index";
	$action 	= "view";

	if(isset($_GET['controller']) && isset($_GET['action'])){
		$controller = $_GET['controller'];
		$action 	= $_GET['action'];
	}

	if(isset($_GET['core'])){
		require_once 'dispatcher.php';
	}else{
		require_once 'view/layout.php';
	}

?>