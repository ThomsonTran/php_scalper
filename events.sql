-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 18, 2019 at 08:48 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php2proj`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `dateEvent` varchar(255) DEFAULT NULL,
  `timeEvent` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `tickets` mediumint(9) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `type`, `dateEvent`, `timeEvent`, `location`, `duration`, `tickets`, `status`) VALUES
(1, 'Curabitur Foundation', 'Concert', '2019-09-03', '18:57:02', 'Great Falls', '06:24:03', 1582, '1'),
(2, 'Venenatis Lacus Company', 'Concert', '2019-09-02', '05:43:56', 'Lampa', '12:29:42', 1769, '1'),
(3, 'Iaculis Enim PC', 'Concert', '2018-08-02', '22:54:41', 'Stirling', '15:04:10', 1226, '1'),
(4, 'Egestas Blandit Corp.', 'Humour', '2019-03-14', '17:29:52', 'San Piero a Sieve', '05:10:05', 1545, '1'),
(5, 'Vehicula Pellentesque Tincidunt Consulting', 'Humour', '2019-01-12', '15:36:01', 'Columbus', '08:38:06', 754, '1'),
(6, 'At Industries', 'Movies', '2019-08-31', '06:04:10', 'Kessenich', '17:57:19', 2958, '1'),
(7, 'Facilisis Non Bibendum Limited', 'Humour', '2018-08-11', '07:26:31', 'Jamioulx', '10:00:06', 2206, '1'),
(8, 'Odio LLC', 'Sports', '2020-03-06', '02:52:26', 'San Floriano del Collio', '00:31:17', 131, '1'),
(9, 'Arcu Associates', 'Sports', '2019-02-05', '16:08:53', 'Khandwa', '12:42:40', 2263, '1'),
(10, 'Ornare LLC', 'Humour', '2020-05-12', '04:10:48', 'Lanklaar', '17:55:18', 2369, '1'),
(11, 'Natoque Company', 'Movies', '2019-08-20', '15:11:22', 'Fayetteville', '14:03:12', 1005, '1'),
(12, 'Magna Nam Ligula Consulting', 'Concert', '2020-02-06', '19:42:47', 'San Gregorio', '13:51:06', 2764, '1'),
(13, 'Lacinia LLP', 'Humour', '2019-04-01', '02:11:40', 'Pont-de-Loup', '04:03:51', 477, '1'),
(14, 'Magna Tellus Faucibus Foundation', 'Sports', '2018-09-16', '04:40:34', 'Calder', '04:02:21', 795, '1'),
(15, 'Gravida Foundation', 'Movies', '2020-01-16', '00:40:34', 'Veraval', '12:27:47', 120, '1'),
(16, 'Aliquet Company', 'Humour', '2019-12-13', '00:49:44', 'Sint-Joost-ten-Node', '17:37:39', 2354, '1'),
(17, 'Metus Inc.', 'Theatrical', '2018-08-25', '04:02:52', 'Llanquihue', '13:23:50', 1983, '1'),
(18, 'Consequat Lectus Corporation', 'Concert', '2018-10-03', '11:30:36', 'Hampstead', '22:50:22', 1560, '1'),
(19, 'Ultrices Posuere LLC', 'Movies', '2020-03-08', '14:43:34', 'Montgomery', '02:24:25', 2036, '1'),
(20, 'Orci Foundation', 'Movies', '2019-06-04', '20:51:04', 'Nieuwenrode', '06:09:31', 805, '1'),
(21, 'Lectus Inc.', 'Concert', '2019-01-04', '22:25:32', 'Ayas', '13:30:21', 1643, '1'),
(22, 'Fusce Industries', 'Movies', '2020-05-04', '03:33:14', 'Toledo', '19:11:05', 2627, '1'),
(23, 'Tincidunt Vehicula Risus LLC', 'Movies', '2019-03-12', '15:13:30', 'Glabais', '02:42:43', 3000, '1'),
(24, 'Lacus Limited', 'Theatrical', '2018-12-27', '23:35:28', 'Llanidloes', '14:27:35', 2753, '1'),
(25, 'Sagittis LLP', 'Theatrical', '2019-09-03', '02:25:52', 'Bondo', '21:25:33', 460, '1'),
(26, 'Tempor LLP', 'Movies', '2020-03-05', '17:05:43', 'Huntingdon', '19:52:24', 2639, '1'),
(27, 'Curabitur Egestas PC', 'Theatrical', '2019-05-24', '16:34:25', 'Beaconsfield', '21:01:18', 38, '1'),
(28, 'Orci LLP', 'Humour', '2018-06-24', '21:05:59', 'Dubuisson', '02:31:58', 403, '1'),
(29, 'Mattis Ornare Foundation', 'Movies', '2020-05-14', '22:03:10', 'Baton Rouge', '09:15:24', 193, '1'),
(30, 'Tincidunt Nunc Corp.', 'Humour', '2018-09-09', '14:06:47', 'Rocky View', '03:32:32', 2291, '1'),
(31, 'Elit Curabitur PC', 'Humour', '2018-10-04', '04:00:15', 'Cerignola', '13:06:30', 45, '1'),
(32, 'Lacus LLP', 'Humour', '2019-03-14', '15:54:34', 'Viano', '08:23:39', 1347, '1'),
(33, 'Duis Sit Amet Company', 'Concert', '2019-01-14', '16:15:04', 'Beauvais', '19:30:00', 1985, '1'),
(34, 'Eget Nisi Dictum LLC', 'Sports', '2019-03-02', '07:22:03', 'Charlottetown', '09:19:07', 2329, '1'),
(35, 'Et Nunc Quisque Company', 'Concert', '2020-01-21', '22:51:37', 'Paternopoli', '09:18:39', 2750, '1'),
(36, 'Ac Metus PC', 'Movies', '2018-10-11', '04:08:22', 'Weißenfels', '01:42:04', 985, '1'),
(37, 'Molestie Tellus Aenean Foundation', 'Concert', '2019-11-03', '23:21:33', 'Yeotmal', '05:48:53', 2652, '1'),
(38, 'Nisi Cum Company', 'Humour', '2019-03-05', '18:29:01', 'Civitacampomarano', '14:02:25', 576, '1'),
(39, 'Proin Corporation', 'Movies', '2018-10-19', '16:33:12', 'Laren', '05:09:43', 57, '1'),
(40, 'Est Mauris Eu Limited', 'Movies', '2019-02-16', '01:35:57', 'Latinne', '03:26:58', 94, '1'),
(41, 'Lorem Auctor Quis Industries', 'Concert', '2019-04-29', '06:03:20', 'Norderstedt', '16:38:22', 211, '1'),
(42, 'Facilisis Magna Tellus Inc.', 'Sports', '2020-01-29', '13:51:02', 'Guysborough', '23:23:44', 2205, '1'),
(43, 'Mauris Company', 'Concert', '2018-10-31', '06:09:17', 'Saint-Jean-Geest', '04:20:27', 2220, '1'),
(44, 'Enim LLC', 'Humour', '2019-09-27', '20:45:14', 'Hagen', '07:10:17', 999, '1'),
(45, 'Vestibulum Ante Ipsum Associates', 'Theatrical', '2020-03-10', '06:04:09', 'GrivegnŽe', '18:46:50', 2591, '1'),
(46, 'Imperdiet Non Vestibulum Incorporated', 'Theatrical', '2020-02-08', '09:32:31', 'Dijon', '02:50:36', 729, '1'),
(47, 'Hendrerit Donec Inc.', 'Sports', '2018-09-15', '09:42:43', 'Torella del Sannio', '05:32:37', 1681, '1'),
(48, 'Non Enim Company', 'Humour', '2019-02-21', '00:29:14', 'Avesta', '15:42:32', 1759, '1'),
(49, 'Lectus Pede Et Corp.', 'Movies', '2018-10-28', '02:10:15', 'Isernia', '01:53:54', 678, '1'),
(50, 'Donec Fringilla Donec Corporation', 'Theatrical', '2018-08-01', '01:05:27', 'Ulhasnagar', '10:06:12', 274, '1'),
(51, 'Facilisis Lorem Tristique Consulting', 'Theatrical', '2019-09-18', '05:50:31', 'Vellore', '10:19:53', 2240, '1'),
(52, 'Montes Nascetur Corporation', 'Concert', '2019-04-16', '03:27:54', 'Prè-Saint-Didier', '09:21:07', 399, '1'),
(53, 'Augue Industries', 'Sports', '2020-04-19', '23:20:46', 'Alto Hospicio', '08:05:25', 607, '1');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
