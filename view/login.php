<div class="container maincontent">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form action="core.php" method="POST">
          <!-- check to see if there an error from the core -->
          <?php if(isset($_GET['error'])): ?>
          <div class="alert alert-danger">
            <strong>Error!</strong> Username/Password don't match!
          </div>
          <?php endif; ?>

          <!-- check to see if there an error from the core -->
          <?php if(isset($_GET['success'])): ?>
          <div class="alert alert-success">
            <strong>Registration succesfull!</strong><br>Please login to continue
          </div>
          <?php endif; ?>

          <div class="form-group">
            <div class="form-label-group">
               <label for="username">Username</label>
              <input type="text" id="username" class="form-control" placeholder="Username" required="required" name="username" autofocus="autofocus">
             
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <label for="inputPassword">Password</label>
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required="required">
              
            </div>
          </div>
          <input class="btn btn-primary btn-block" type="submit" name="login" value="login">
        </form>

      </div>
    </div>
  </div>