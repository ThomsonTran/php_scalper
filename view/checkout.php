<?php if(isset($_SESSION['user'])): ?>

  <div class="container maincontent mb-5">
    <div class="card card-register mx-auto mt-5">


    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Personal Information</div>
      <div class="card-body">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                <label for="name">First Name</label>
                  <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First name" required="required" autofocus="autofocus" value = "<?=$_SESSION['user']['firstname'] ?>">
              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <label for="lastname">Last name</label>
                  <input type="text" id="lastname" name="lastname" class="form-control"  value = "<?=$_SESSION['user']['lastname'] ?>" placeholder="Last name" required="required">
                  
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
               <label for="address">Address</label>
              <input type="text" id="address" name="address" class="form-control" placeholder="Address" required="required" value = "<?=$_SESSION['user']['address'] ?>">
          
            </div>
          </div>


         <div class="form-group">
            <div class="form-label-group">
               <label for="province">Province</label>
              <input type="text" id="province" name="province" class="form-control" placeholder="Province" required="required" value = "<?=$_SESSION['user']['province'] ?>">
           </div>
          </div>


          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                   <label for="city">City</label>
                  <input type="text" id="city" name="city"  value = "<?=$_SESSION['user']['city'] ?>"class="form-control" placeholder="City" required="required">
                 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                    <label for="postal">Postal Code</label>
                     <input type="text" id="postal" name="postal"  value = "<?=$_SESSION['user']['postal'] ?>"class="form-control" placeholder="Postal Code" required="required">
                 </div>
              </div>
            </div>
          </div>

         <div class="form-group">
            <div class="form-label-group">
               <label for="telephone">Telephone</label>
              <input type="text" id="telephone" name="telephone" class="form-control" value = "<?=$_SESSION['user']['telephone'] ?>" placeholder="Telephone" required="required">
           </div>
          </div>


         <div class="form-group">
            <div class="form-label-group">
               <label for="email">Email</label>
              <input type="email" id="email" name="email"  value = "<?=$_SESSION['user']['email'] ?>" class="form-control" placeholder="Email" required="required">
           </div>
          </div>
        </div>
      </div>

    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Credit Card Information</div>
      <div class="card-body">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                <label for="name">Credit Card Number</label>
                  <input type="text" id="cc_number" name="cc_number" value = "<?=$_SESSION['user']['cc_number'] ?>" class="form-control" placeholder="Credit Card Number" required="required" autofocus="autofocus">
              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <label for="lastname">Credit Card Type</label>
                  <input type="text" id="cc_type" name="cc_type" class="form-control" value = "<?=$_SESSION['user']['cc_type'] ?>" placeholder="Credit Card Type" required="required">
                  
                </div>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                   <label for="city">Expiration Date</label>
                  <input type="text" id="cc_expiration" name="cc_expiration" class="form-control" value = "<?=$_SESSION['user']['cc_expiration'] ?>" placeholder="Expiration" required="required">
                 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                    <label for="csc"> CSC</label>
                     <input type="text" id="csc" name="csc" value = "<?=$_SESSION['user']['csc'] ?>" class="form-control" placeholder="csc" required="required">
                 </div>
              </div>
            </div>
          </div>
          <input class="btn btn-primary btn-block" type="hidden"  name="register" value="Register">
         <input class="btn btn-primary btn-block" type="button" onClick="confSubmit(this.form);"  name="checkout" value="Checkout">
          </div>
      </div>
   </form>
 </div>
</div>

 <?php else: ?>


  <div class="container maincontent mb-5">
    <div class="card card-register mx-auto mt-5 pb-5">

    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Personal Information</div>
      <div class="card-body">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                <label for="name">First Name</label>
                  <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <label for="lastname">Last name</label>
                  <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last name" required="required">
                  
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
               <label for="address">Address</label>
              <input type="text" id="address" name="address" class="form-control" placeholder="Address" required="required">
          
            </div>
          </div>


         <div class="form-group">
            <div class="form-label-group">
               <label for="province">Province</label>
              <input type="text" id="province" name="province" class="form-control" placeholder="Province" required="required">
           </div>
          </div>


          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                   <label for="city">City</label>
                  <input type="text" id="city" name="city" class="form-control" placeholder="City" required="required">
                 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                    <label for="postal">Postal Code</label>
                     <input type="text" id="postal" name="postal" class="form-control" placeholder="Postal Code" required="required">
                 </div>
              </div>
            </div>
          </div>

         <div class="form-group">
            <div class="form-label-group">
               <label for="telephone">Telephone</label>
              <input type="text" id="telephone" name="telephone" class="form-control" placeholder="Telephone" required="required">
           </div>
          </div>


         <div class="form-group">
            <div class="form-label-group">
               <label for="email">Email</label>
              <input type="email" id="email" name="email" class="form-control" placeholder="Email" required="required">
           </div>
          </div>
        </div>
      </div>

    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Credit Card Information</div>
      <div class="card-body">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                <label for="name">Credit Card Number</label>
                  <input type="text" id="cc_number" name="cc_number" class="form-control" placeholder="Credit Card Number" required="required" autofocus="autofocus">
              
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <label for="lastname">Credit Card Type</label>
                  <input type="text" id="cc_type" name="cc_type" class="form-control" placeholder="Credit Card Type" required="required">
                  
                </div>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                   <label for="city">Expiration Date</label>
                  <input type="text" id="cc_expiration" name="cc_expiration" class="form-control" placeholder="Expiration" required="required">
                 
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                    <label for="csc"> CSC</label>
                     <input type="text" id="csc" name="csc" class="form-control" placeholder="csc" required="required">
                 </div>
              </div>
            </div>
          </div>
          <input class="btn btn-primary btn-block" type="hidden"  name="register" value="Register">
         <input class="btn btn-primary btn-block" type="button" onClick="confSubmit(this.form);"  name="checkout" value="Checkout">
          </div>
      </div>
   </form>
 </div>
 </div>

 <?php endif; ?>


<script type="text/javascript">

function confSubmit(form) {
if (confirm("Are you sure you wish to make this purchase?")) {
form.submit();
} else{
 window.location=("index.php?controller=menu&action=cart");

}

}
</script>