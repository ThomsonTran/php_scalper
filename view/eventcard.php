


   <div class="row">

    <?php foreach ($events as $eventdata): ?>
   
      <div class="col-lg-4 mb-4">
      <a class= "custom-card" href ="?controller=purchase&action=view&id=<?=$eventdata -> getID()?>">
        <div class="card h-100">
          <h4 class="card-header"> <?= $eventdata -> getName() ?></h4>
          <div class="card-body">
            <p class="card-text"> 
              Type: <?=  $eventdata -> getType() ?>
              <br>
              Date: <?= $eventdata -> getDateEvent();  ?>
              <br>
              Time: <?=  $eventdata -> getTimeEvent();  ?>
              <br>
              Location: <?=  $eventdata -> getLocation();  ?>
              <br>
              duration: <?=  $eventdata -> getDuration();  ?>
              <br>
              Tickets Left: <?=  $eventdata -> getTickets();  ?>
              <br>
              

            </p>
          </div>
        </div>
         </a>

      </div>
   
    <?php endforeach ?>
  </div>




    <!-- /.row -->
