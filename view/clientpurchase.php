<?php include_once "header.php"; ?>


  <!-- Page Content -->
  <div class="container maincontent">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3"> <?= $event -> getName(); ?>
      <!-- <small>Subheading</small> -->
    </h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php">Home</a>
      </li>
      <li class="breadcrumb-item active"> <?= $event -> getName(); ?></li>
    </ol>

    <!-- Portfolio Item Row -->
    <div class="row">

      <div class="col-md-8">
        <img class="img-fluid" src="http://placehold.it/750x500" alt="">
      </div>

      <div class="col-md-4">
        <h3 class="my-3">Event Description</h3>
        <?php include "timer.php" ?>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.</p>

        <h3 class="my-3">Event Details</h3>
        <ul>
               Type: <?=  $event -> getType() ?>
              <br>
              Date: <?= $event-> getDateEvent();  ?>
              <br>
              Time: <?=  $event -> getTimeEvent();  ?>
              <br>
              Location: <?=  $event-> getLocation();  ?>
              <br>
              duration: <?=  $event -> getDuration();  ?>
              <br>
              Tickets Left: <?=  $event -> getTickets();  ?>
              <br>
        </ul>

       

        <form class="" action="core.php" method="POST">
        <input type="number" name="quantity" min="1" max="50" value= "1">
         <input type="hidden" name="addtocart" value="addtocart">
        <button type="submit" class="btn btn-success btn-block" value="<?=$event-> getID()?>" name="cart">Add to Cart</button>
      </form>
      </div>


    </div>
    <!-- /.row -->

<div class="mb-4"></div>
    <!-- /.row -->

  </div>
  <!-- /.container -->


<?php include_once "footer.php"; ?>